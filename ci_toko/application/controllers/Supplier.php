<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model("supplier_model");
	}
	public function index()
	{
		$this->listSupplier();
	}
	public function listSupplier()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$this->load->view('supplier', $data);
	}
	public function input_supplier()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
			if (!empty($_REQUEST)) {
				$m_supplier = $this->supplier_model;
				$m_supplier->save();
				redirect("supplier/index", "refresh");	
			}
		
		$this->load->view('input_supplier', $data);
	}
	public function detailSupplier($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		$this->load->view('detail_supplier', $data);	
	}
	
}
