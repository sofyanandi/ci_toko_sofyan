<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model("jabatan_model");
	}
	public function index()
	{
		$this->listJabatan();
	}
	public function listJabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$this->load->view('jabatan', $data);
	}
	
	public function input_jabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
			if (!empty($_REQUEST)) {
				$m_jabatan = $this->jabatan_model;
				$m_jabatan->save();
				redirect("jabatan/index", "refresh");	
			}
		
		$this->load->view('input_jabatan', $data);
	}
	public function detailJabatan($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$this->load->view('detail_jabatan', $data);	
	}
}
