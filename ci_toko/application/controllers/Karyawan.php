<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	
	public function __construct()
	{
		parent:: __construct();
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");
	}
	public function index()
	{
		$this->listKaryawan();
	}
	public function listKaryawan()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$this->load->view('karyawan', $data);
	}
	public function input_karyawan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
			if (!empty($_REQUEST)) {
				$m_karyawan = $this->karyawan_model;
				$m_karyawan->save();
				redirect("karyawan/index", "refresh");	
			}
		
		$this->load->view('input_karyawan', $data);
	}
	public function detailKaryawan($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$this->load->view('detail_karyawan', $data);	
	}
	
}
