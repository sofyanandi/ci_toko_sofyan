<style type="text/css">
*{margin:0; padding:0;}
 
body {
	background-color:#999;
	font-family:Arial, Helvetica, sans-serif;
	color:#FFF;
}
 
.menu-wrap {
	background-color:#F96; 
	height:50px; 
	line-height:50px; 
	position:relative;
	width:100%;
	margin:auto;
	margin-top:20px;
}
 
.menu-wrap ul {
	list-style:none;
}
 
.menu-wrap ul li a {
	float:left; 
	width:160px; 
	display:block; 
	text-align:center; 
	color:#FFF; 
	text-decoration:none; 
	text-transform:uppercase;
}
 
.menu-wrap ul li a:hover {
	background-color:#666; 
	display:block;
}

.menu-wrap ul li:hover ul {
	display:block;
}
 
.menu-wrap ul ul {
	display:none; 
	list-style:none; 
	position:absolute; 
	background-color:#F96;
	left:290px; 
	top:50px; 
	width:190px;
}
 
.menu-wrap ul ul li a {
	float:none; 
	display:block; 
	padding-left:30px; 
	text-align:left; 
	width:160px;
}
 
.menu-wrap ul ul li a:hover {
	color:#fff;
}
</style>

<body>
	<h1 align="center">Toko Jaya Abadi</h1>
    
   <div class="menu-wrap">
	<ul>
		<li><a href="<?=base_url();?>home">Home</a></li>
		<li><a href="#">Laporan</a></li>
		<li><a href="#">Data</a>
			<ul>
				<li><a href="<?=base_url();?>karyawan/listkaryawan">Karyawan</a></li>
				<li><a href="<?=base_url();?>jabatan/listjabatan">Jabatan</a></li>
                <li><a href="<?=base_url();?>barang/listbarang">Barang</a></li>
                <li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Jenis Barang</a></li>
                <li><a href="<?=base_url();?>supplier/listsupplier">Supplier</a></li>
			</ul>
		</li>
        <li><a href="tentang.html">Transaksi</a></li>
        <li><a href="tentang.html">Logout</a></li>
	</ul>
  </div>
</body>