<?php
	foreach($detail_karyawan as $data) 
	{
    	$nik			= $data->nik;
		$nama_lengkap	= $data->nama_lengkap;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tgl_lahir;
		$jenis_kelamin	= $data->jenis_kelamin;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$kode_jabatan	= $data->kode_jabatan;
    }
?>
<body bgcolor="#FFFFCC">
<h1 align="center">Detail Karyawan</h1>
<table border="0" align="center" width="30%" bgcolor="#FFFFFF">
<tr>
	<td>NIK </td>
    <td>:</td>
    <td><?= $nik;?></td>
</tr>
<tr>
    <td>Nama Karyawan </td>
    <td>:</td>
    <td><?= $nama_lengkap;?></td>
</tr>
<tr>
    <td>Tempat Lahir </td>
    <td>:</td>
    <td><?= $tempat_lahir;?></td>
</tr>
<tr>
    <td>Tanggal Lahir </td>
    <td>:</td>
    <td><?= $tgl_lahir;?></td>
</tr>
<tr>
    <td>Jenis Kelamin </td>
    <td>:</td>
    <td><?= $jenis_kelamin;?></td>
 </tr>
 <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><?= $alamat;?></td>
  </tr>
  <tr>
    <td>Telp</td>
    <td>:</td>
    <td><?= $telp;?></td>
</tr>
</table>
</body>