<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang_model extends CI_Model 
{
	private $_table = "jenis_barang";
	
	public function tampilDataJenis_barang()
	{
		return $this->db->get($this->_table)->result();
	}
	public function tampilDataJenis_barang2()
	{
		$query =$this->db->query("SELECT * FROM jenis_barang WHERE flag = 1");
		return $query->result();
	}
	public function tampilDataJenis_barang3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_jenis','ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
		
	public function detail($kode_jenis)
	{
		$this->db->select('*');
		$this->db->where ('kode_jenis', $kode_jenis);
		$this->db->where('flag',1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save ()
	{
		
		$data['kode_jenis']			= $this->input->post('kode_jenis');
		$data['nama_jenis']			= $this->input->post('nama_jenis');
		$data['flag']				= 1;
		$this->db->insert($this->_table, $data);
	}
}