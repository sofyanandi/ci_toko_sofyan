<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model 
{
	private $_table = "barang";
	
	public function tampilDataBarang()
	{
		return $this->db->get($this->_table)->result();
	}
	public function tampilDataBarang2()
	{
		$query =$this->db->query("SELECT * FROM barang WHERE flag = 1");
		return $query->result();
	}
	public function tampilDataBarang3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_barang','ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
	public function detail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where ('kode_barang', $kode_barang);
		$this->db->where('flag',1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save ()
	{
		$data['kode_barang']			= $this->input->post('kode_barang');
		$data['nama_barang']			= $this->input->post('nama_barang');
		$data['harga_barang']			= $this->input->post('harga_barang');
		$data['kode_jenis']				= $this->input->post('kode_jenis');
		$data['flag']					= 1;
		$this->db->insert($this->_table, $data);
	}
}